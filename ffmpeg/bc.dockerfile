FROM ubuntu:22.04

ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/London"
ENV LLVM_COMPILER=clang

RUN apt-get update && apt-get install -y clang-12 build-essential \
  python3 python3-pip file pkg-config
RUN pip3 install --no-cache-dir wllvm
RUN ln -s /usr/bin/clang-12 /usr/bin/clang && \
  ln -s /usr/bin/clang++-12 /usr/bin/clang++ && \
  ln -s /usr/bin/llvm-link-12 /usr/bin/llvm-link

COPY ./ffmpeg-6.1/ /ffmpeg/
COPY ./bc.sh /ffmpeg/
WORKDIR /ffmpeg/
RUN /bin/bash -c "/ffmpeg/bc.sh"

