#!/bin/sh
DISABLE_LAVA=1 ./configure --disable-x86asm --prefix="./build" --cc="/llvm-lava/build/bin/clang" --disable-optimizations --disable-stripping --enable-debug=3 --extra-ldflags="-lmemorizer" --host-cc="gcc" \
	--samples=fate-suite/ \
	--disable-ffplay --disable-ffprobe --disable-doc \
	--disable-yasm --disable-everything --enable-encoder=libx264 --enable-encoder=libfaac --enable-decoder=h264 --enable-muxer=mp4 --enable-demuxer=h264 --enable-parser=h264 --enable-protocol=file

make -j12
export SOFTCAPS=1
/scripts/parse_perm.py -i ffmpeg/ffmpeg -p /root/ffmpeg.json -e -S "/ffmpeg/" -o ffmpeg.pcg
make fate-rsync
time make fate | tee /root/result.txt
