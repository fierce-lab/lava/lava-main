FROM lava

RUN apt-get update && apt-get install -y \
  rsync time

COPY ./ffmpeg-6.1/ /ffmpeg
COPY ./test.sh /ffmpeg/test.sh
COPY ./ffmpeg.json /root/ffmpeg.json

ENV OBJENC_BASE="/scripts"
ENV MEMORIZER_BASE="/libmemorizer"
ENV NGX_PCG="/root/ffmpeg.json"

WORKDIR /ffmpeg
RUN bash test.sh
