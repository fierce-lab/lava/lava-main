#!/bin/sh

cd /ffmpeg/
./configure --disable-x86asm --prefix="./build" --cc="wllvm" --disable-optimizations --enable-debug --disable-stripping \
	--disable-ffplay --disable-ffprobe --disable-doc \
	--disable-yasm --disable-everything --enable-encoder=libx264 --enable-encoder=libfaac --enable-decoder=h264 --enable-muxer=mp4 --enable-demuxer=h264 --enable-parser=h264 --enable-protocol=file
make -j12
extract-bc ffmpeg
