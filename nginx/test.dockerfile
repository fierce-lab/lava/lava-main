FROM lava

RUN apt-get update && apt-get install -y \
  libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
RUN apt-get install -y apache2-utils

COPY ./nginx-1.23.1/ /nginx
COPY ./nginx.json /root/nginx.json

ENV OBJENC_BASE="/scripts"
ENV MEMORIZER_BASE="/libmemorizer"
ENV NGX_PCG="/root/nginx.json"

RUN bash -c "cd /nginx/scripts; ./benchmark.sh --use-cc=$(which clang) | tee /root/result.txt"

