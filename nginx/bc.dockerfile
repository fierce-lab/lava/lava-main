FROM ubuntu:22.04

RUN apt-get update && apt-get install -y \
  python3 python3-pip clang-12 file \
  libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev

RUN pip install --no-cache-dir wllvm

ENV LLVM_COMPILER=clang

COPY ./nginx-1.23.1 /nginx

WORKDIR /nginx
RUN ln -s /usr/bin/clang-12 /usr/bin/clang \
  && ln -s /usr/bin/llvm-link-12 /usr/bin/llvm-link; \
  export LLVM_COMPILER=clang; \
  ./configure --builddir=/nginx/build --with-cc="/usr/local/bin/wllvm" --with-cc-opt="-g -O0" \
  && make -j12 \
  && extract-bc /nginx/build/nginx

