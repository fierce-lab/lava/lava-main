FROM ubuntu:22.04

RUN apt-get update && apt-get install -y \
  python3-pip nodejs npm ninja-build cmake \
  llvm-12-dev wget unzip z3 \
  pkg-config

RUN pip install wllvm

COPY ./objenc/analysis /scripts
COPY ./libmemorizer /libmemorizer
COPY ./llvm-lava /llvm-lava

RUN bash -c "cd /libmemorizer && make && make install; cd /llvm-lava && make"
RUN ln -s /llvm-lava/build/bin/clang-12 /usr/bin/clang-12 && \
  ln -s /llvm-lava/build/bin/clang-12 /usr/bin/clang && \
  pip install --no-cache-dir typing_extensions pydantic

ENV LD_LIBRARY_PATH=/usr/local/lib
