# Running Softcaps with Examples

The procedure of compiling and running a program with LAVA compiler and Softcaps Library follows:

![LAVA Pipeline.drawio](softcaps.assets/LAVA%20Pipeline.drawio.png)

1. **IR**: Compile the source code to obtain the linked LLVM IR of the source code.
2. **PCG**: Running the Object-Encapsulation Analysis path to obtain the Program Capability Graph with file tags
3. **Softcaps**: Compile and install the softcaps library (libmemorizer) into `/usr/local/lib/`
4. **Runtime**: Compile the source code with LAVA CC and link the softcaps library (libmemorizer) to obtain the LAVA Runtime
5. **Binary**: Encode the Program Capability Graph into the runtime and produce the final binary.
6. **Running**: Set the environment variable to enable the softcaps instrumentation during execution.

## Setup

Beforehand, the global `CFLAGS` will be set to `CFLAGS="-O0 -g"`.

## IR

Although you can compile every C source code using the `CFLAGS+=-S -emit-llvm` to compile evey source code into `.bc` LLVM bitcode and use `llvm-link` to link them together, it is not recommended as it brings much complexity with build systems such as `autoconf`, `cmake`, or even the simpliest `make` system.

The easy way to build a combined LLVM bitcode for the entire program is to use the `wllvm` script [https://github.com/travitch/whole-program-llvm](https://github.com/travitch/whole-program-llvm). It can be obtained using `pip`.

> pip install --no-cache-dir wllvm

Using `wllvm`, you shall set `CC=wllvm` and `LLVM_COMPILER=clang`. Additionally, if your system use `clang-12` and `llvm-link-12` instead of `clang` and `llvm-link`, you should set `LLVM_CC_NAME="clang-12"` and `LLVM_LINK_NAME="llvm-link-12"`.

With the `wllvm` correctly configured, you shall be able to obtain the same binary file. If your binary file is `${BINARY}`, you can use `extract-bc ${BINARY}` to obtain `${BINARY}.bc`, which is the combined LLVM bitcode that is later used in analyzing the program capability graph.

## PCG

You shall use the docker image `2000jedi/pcg:latest` to build a program capability graph from `${BINARY}.bc`. You can use the following script as an example:

```bash
docker run -it --rm -v ."$(pwd)":/app 2000jedi/pcg:latest` /bin/bash
/objenc/build/objenc `${BINARY}.bc` `${BINARY}.json`
exit
```

Your program capability graph will be `${BINARY}.json`

## Softcaps

You shall compile and install softcaps with the following make command:

> make && sudo make install

## Runtime

You shall build the LAVA CC in the repository: [https://gitlab.com/fierce-lab/llvm-lava/](https://gitlab.com/fierce-lab/llvm-lava/).

> make

The compiler is under `llvm-lava/build/bin/clang-12`

You shall compile the runtime setting `CC=${readlink -f llvm-lava/build/bin/clang-12} ` and `LIBS+=-lmemorizer`

## Binary

The next step is to encode the program capability graph into the LAVA Runtime. You can run the script to encode the PCG into the binary to get a fully executable binary.

> ./objenc/analysis/parse_perms.py --bin ${BINARY} --pcg \${BINARY}.json --embed-inplace

## Running

You need to define the environment`export SOFTCAPS=1` to run the program to turn on softcaps.

