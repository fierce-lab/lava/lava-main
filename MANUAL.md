# LAVA MANUAL

## Install dependencies

``` bash
sudo apt install python3-pip nodejs npm ninja-build cmake \
  llvm-12-dev wget unzip z3 pkg-config libjsoncpp-dev
sudo pip install wllvm
pip install --no-cache-dir typing_extensions pydantic
```

## Build the LAVA toolchain

The sequence of the following tools does not matter.

### Object-Encapsulation Tool

This tool is used to generate PCG from the LLVM bytecode.

``` bash
cd objenc/pcggen
./build.sh
```

### LibMemorizer

This library is the implementation of LAVA VM, which supports memory operation tracing and PCG permission enforcement (i.e., SoftCaps).

``` bash
cd libmemorizer
make
sudo make install
```

If you want to enable verbose mode, use `make lib-verbose` instead of `make`.

If you do not want to install the library or you do not have root permission, you can specify `LD_LIBRARY_PATH` to the library path.

``` bash
export LD_LIBRARY_PATH=$(pwd)/build/lib:$LD_LIBRARY_PATH
```

### LAVA Clang

This tool is used to compile C programs for LAVA VM. `-lmemorizer` is required in the linking stage.

``` bash
cd llvm-lava
make
sudo make install
```

If you do not want to install the compiler or you do not have root permission, you can specify `PATH` to the compiler path.

``` bash
export PATH=$(pwd)/build/bin:$PATH
```

## Using the LAVA toolchain

### Obtain the PCG

``` bash
export LLVM_COMPILER=clang
wllvm -g -O0 ANY_FILE.c -o ANY_BIN
extract-bc ANY_BIN
objenc/pcggen/build/objenc ANY_BIN.bc > ANY_PCG.json
```

Note that `ANY_BIN` generated in this step is not used for execution. It is only used to obtain the bytecode `ANY_BIN.bc`.

### Compile and run the C program

``` bash
clang-12 -g -O0 ANY_FILE.c -o ANY_BIN -lmemorizer
./ANY_BIN
```

The optimization level should be the same as the one used in the previous step.

If you want to disable instrumentation, export `DISABLE_LAVA=1` before compiling. (Then the compiler works as a normal clang compiler)

### Analyze the PCG

``` bash
python3 objenc/analysis/analysis.py ANY_PCG.json
```

### Run the program with SoftCaps

1. Parse the PCG into the binary

``` bash
python3 objenc/analysis/parse_perm.py -e -i ANY_BIN -p ANY_PCG.json
```

> NOTE: If the path in the PCG does not match the path of the source file, you need to strip the path by passing `-s` to the script.
> 
> e.g.: The source file of `ANY_BIN` is `folder1/ANY_FILE.c`, but the path in the PCG is `folder2/ANY_FILE.c`, then you need to run the following command:
>
> ``` bash
> python3 objenc/analysis/parse_perm.py -e -i ANY_BIN -p ANY_PCG.json -s folder2
> ```

2. Run the program

``` bash
export SOFTCAPS=1
./ANY_BIN
```
