.PHONY: submodules pcg lava ffmpeg nginx clean mrproper

submodules:
	git submodule update --init --recursive

pcg:
	docker build -t pcg -f ./objenc/pcggen/Dockerfile ./objenc/pcggen/

pcg-shell: pcg
	docker run -it --name pcg-shell -v .:/lava-main -w /lava-main pcg /bin/bash

lava:
	docker build -t lava -f lava.dockerfile .

lava-shell: lava
	docker run -it --name lava-shell -v .:/lava-main -w /lava-main lava /bin/bash

TARGETS = ffmpeg nginx

examples: pcg lava $(TARGETS)

define make-target
$(info making target: $1)
$(info Path to .bc file: $(BCPATH_$(1)))

$1/$1.bc:
	docker build -t lava-$1 -f $1/bc.dockerfile ./$1/
	sh -c "docker cp `docker create --name lava-$1 lava-$1`:$(BCPATH_$(1)) ./$1/$1.bc && docker rm lava-$1"

$1/$1.json: $1/$1.bc
	docker build -t pcg-$1 -f $1/pcg.dockerfile ./$1/
	sh -c "docker cp `docker create --name pcg-$1 pcg-$1`:/root/$1.json ./$1/$1.json && docker rm pcg-$1"

$1: $1/$1.json
	docker build -t test-$1 -f $1/test.dockerfile ./$1
	sh -c "docker cp `docker create --name test-$1 test-$1`:/root/result.txt ./$1/result.txt && docker rm test-$1"
endef

BCPATH_ffmpeg = /ffmpeg/ffmpeg.bc
BCPATH_nginx = /nginx/build/nginx.bc

$(foreach tgt,$(TARGETS),$(eval $(call make-target,$(tgt))))

clean:
	docker rm -f $(addprefix lava-,$(TARGETS))
	docker rm -f $(addprefix pcg-,$(TARGETS))
	docker rm -f $(addprefix test-,$(TARGETS))
	docker rmi -f $(addprefix lava-,$(TARGETS))
	docker rmi -f $(addprefix pcg-,$(TARGETS))
	docker rmi -f $(addprefix test-,$(TARGETS))

clean-all: clean
	rm -f $(patsubst %,%/*.json,$(TARGETS))
	rm -f $(patsubst %,%/*.bc,$(TARGETS))

clean-shells:
	docker rm -f pcg-shell
	docker rm -f lava-shell

mrproper: clean-all clean-shells
	docker rmi -f pcg lava

